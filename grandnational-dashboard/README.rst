***************************************
Grand National 2020 Countdown Dashboard
***************************************

This repo contains all files for the Grand National 2020 Countdown Dashboard.

.. image:: https://git.nonprod.williamhill.plc/wh_capacity/grandnational-dashboard/uploads/47fdb6cc5b3a6658ee884ab148fb8a29/image.png
  :width: 40%


The dashboard is installed on ``sc1uxpremn81`` running in docker.

The dashboard uses ``python`` as an ASGI server, ``css`` for formatting, and ``jQuery`` for front-end scripts.

We use `uvicorn <https://www.uvicorn.org>`_ as an ASGI server and `gunicorn <https://gunicorn.org>`_ to manage the asynchronous workers. We use `bootstrap <https://getbootstrap.com/>`_ for front-end formatting and, `jQuery <https://jquery.com/>`_ for front-end scripts. We use `npm <https://www.npmjs.com>`_, `bower <https://bower.io/>`_ and `poetry <https://python-poetry.org>`_ to manage dependencies.

Optionally we can use `docker <https://www.docker.com/>`_ to orchestrate uvicorn and guvicorn.


Running Locally
===============

Requirements
------------

You will need installed:

- python>=3.6.2
- npm
- docker `(optional)`
  
on your machine as a pre-requisite. Versions of python lower than 3.6.2 may work, but you will have to delete the ``poetry.lock`` file before installing the python dependencies. Versions lower than 3.6.2 will not come with `venv` - you will need to install an alternative (``virtualenv``) or use ``potery`` to manage the virtual environment for python.

Installation Steps
------------------

Create workspace
^^^^^^^^^^^^^^^^

On your machine clone the repo:

``cd ~``

``git clone https://git.nonprod.williamhill.plc/wh_capacity/grandnational-dashboard.git``

In this folder, create a virtual environment for python:

``cd ~/grandnational-dashboard``

``python3 -m venv gn-dashboard``

Activate this environment:

``source gn-dashboard/bin/activate``


Install Dependencies
^^^^^^^^^^^^^^^^^^^^

Install ``bower`` using ``npm``:

``npm install -g bower``

Install poetry:

``pip install poetry``

Install the node depdencies with ``npm``:

``npm install``

`this reads the dependencies from package.json`.

Install the bower dependencies with ``bower``:

``bower install``

`this reads the dependencies from bower.json`.

Install the python dependencies with poetry:

``poetry install --no-dev``

`this reads the dependencies from pyproject.toml and poetry.lock`

Copy The Large Static Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The video used as a banner is too large for gitlab. You should do:

``cd static/assets/videos``

and

``wget http://sc1uxpremn81:9001/static/assets/videos/gn19-short.mp4``.

Alternatively, if the dashboard isn't online, you can download the file manually from confluence:

https://conf.willhillatlas.com/display/CME/Static+Files

Place this video locally in ``static/assets/videos/`` saved as ``gn19-short.mp4``.

Run the Webserver
^^^^^^^^^^^^^^^^^

Manually
""""""""

Running the webserver is done with ``uvicorn``. Go into ``grandnational_dashboard`` and launch uvicorn:

``cd grandnational_dashboard``

``uvicorn main:app``

(The folder name is the same as the repo name. This may be confusing but is intentional when creating a project with poetry without a src folder. The full path you should be in is ``~/grandnational-dashboard/grandnational_dashboard`` and there should be a ``main.py`` file inside this folder.)

Your dashboard should then be available at http://127.0.0.1:8000


Docker
""""""

We can use Docker for deployment. To do this go back to the root of the project and build the docker image from the ``Dockerfile``.

``cd ~/grandnational-dashboard``

``docker build . -t gn-20-dash``

When this has built you can run the container with:

``docker run --rm --name dashboard -p 8000:80 gn-20-dash:latest``

Your dashboard will then be available at http://127.0.0.1:8000.

This docker image runs gunicorn to manage the asynchronous workers. It's scaleable, and will smartly use the resources of the machine it's running on.
