$('#gn-countdown').countdown('2020/04/04 17:15', function(event) {
  $(this).html(event.strftime('%D days %Hhrs %Mm %Ss'));
});

$('#chelt-countdown').countdown('2020/03/10 13:30', function(event) {
  $(this).html(event.strftime('%D days %Hhrs %Mm %Ss'));
});

$("#event-rotate").Morphist({
    // The entrance animation type (In).
    animateIn: "zoomInDown",
    // The exit animation type (Out). Refer to Animate.css for a list of available animations.
    animateOut: "zoomOutRight",
    // The delay between the changing of each object in milliseconds.
    speed: 5000,
    complete: function () {
        // Called after the entrance animation is executed.
    }
});

