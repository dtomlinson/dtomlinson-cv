from starlette.applications import Starlette
from starlette.templating import Jinja2Templates
from starlette.routing import Route, Mount
from starlette.staticfiles import StaticFiles


templates = Jinja2Templates(directory='templates')


async def home(request):
    return templates.TemplateResponse(
        'index.html',
        {'request': request},
    )

routes = [
    Route('/', endpoint=home),
    Mount('/static', StaticFiles(directory='../static'), name='static'),
    Mount(
        '/node_modules',
        StaticFiles(directory='../node_modules'),
        name='node_modules',
    ),
    Mount(
        '/bower_components',
        StaticFiles(directory='../bower_components'),
        name='bower_components',
    ),
]

app = Starlette(debug=True, routes=routes)
