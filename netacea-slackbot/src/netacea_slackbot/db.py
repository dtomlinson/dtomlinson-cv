from os import path, urandom
import hashlib
from typing import Tuple
import toml
import io

from pylite.simplite import Pylite

from netacea_slackbot import CONFIG, CONFIG_PATH
# from netacea_slackbot.config import Config
from netacea_slackbot.__header__ import __header__


class Dev:
    @property
    def hash(self):
        try:
            if not self._hash_exists:
                pass
        except AttributeError:
            self._hash = hashlib.pbkdf2_hmac(
                'sha256',
                self.entry[self.name].encode('utf-8'),
                self.salt,
                100000,
            )
            self._hash_exists = True
        finally:
            return self._hash

    @property
    def salt(self):
        self._salt = urandom(32)
        return self._salt

    @staticmethod
    def as_string(obj: bytes) -> str:
        return bytes.hex(obj)

    @staticmethod
    def fromhex(obj: str) -> bytes:
        return bytes.fromhex(obj)

    @staticmethod
    def _from_key(config_var) -> Tuple[str, str]:
        header, name = config_var.split('.')
        return (header, name)

    def __init__(
        self, config_path: str, config_contents: dict, config_var: str
    ):
        self.table: str = __header__
        self.config_path = config_path
        self.config_contents = config_contents
        self.header = self._from_key(config_var)[0]
        self.name = self._from_key(config_var)[1]
        self.entry = self.config_contents[self.table][self.header]
        # super().__init__()

    def get_database_file(self):
        self.database = CONFIG.db_path
        self.database += (
            f'.{self.table}.db'
            if CONFIG.db_path[-1] == '/'
            else f'/.{self.table}.db'
        )
        self.database = path.expanduser(self.database)
        return self

    def open_database(self):
        self.database = Pylite(self.database)
        print('\n', self.database)

    def get_table(self):
        tables = [i[0] for i in self.database.get_tables()]
        if self.table not in tables:
            print('creating')
            self.database.add_table(
                f'{self.table}',
                Name='text',
                Hash='text',
                Salt='text',
                Value='text',
            )
        else:
            print('exists')
        self.table_name = self.table

    def _check_entries(self):
        var = self.database.get_items(self.table, f'Name="{self.name}"')
        if len(var) == 0:
            return False
        else:
            return True

    def insert_entries(self):
        self.database.insert(
            self.table,
            self.name,
            self.as_string(self.hash),
            self.as_string(self.salt),
            self.entry[self.name],
        )

    def update_entries_in_db(self):
        self.database.remove(self.table, f'Name="{self.name}"')
        self.insert_entries()

    def run_query(self, query: str):
        cur = self.database.db.cursor()
        cur.execute(query)
        self.database.db.commit()
        self.result = cur.fetchall()
        return self

    def get_all_items(self, where_clause: str = None):
        if where_clause is not None:
            self.result = inst.database.get_items(self.table, where_clause)
        else:
            self.result = inst.database.get_items(self.table)
        return self

    def decode_items(self):
        print(self.result)

    def process(self):
        if not self._check_entries():
            print('does not exist')
            self.insert_entries()
            self.update_entries_in_config()
            self.get_all_items()
            print(f'returning: {self.result[0][3]}')
            return self.entry[self.name]
        else:
            self.get_all_items()
            if self.result[0][1] == self.entry[self.name]:
                print('exists and hash matches')
                print(f'returning: {self.result[0][3]}')
                return self.result
            else:
                print('exists and hash doesnt match')
                print(
                    f'file_hash={self.entry[self.name]}, {self.result[0][1]}'
                )
                self.update_entries_in_db()
                self.update_entries_in_config()
                self.get_all_items()
                print(f'returning: {self.result[0][3]}')
                return self.entry[self.name]

    def _open_config_file(self) -> io.TextIOWrapper:
        self.config_path += (
            '/config.toml' if self.config_path[-1] != '/' else 'config.toml'
        )
        c = open(path.expanduser(self.config_path), 'w')
        return c

    def update_entries_in_config(self):
        self.entry.update({self.name: self.as_string(self.hash)})
        print(self.config_contents)
        print(self.entry)
        c = self._open_config_file()
        toml.dump(self.config_contents, c)
        c.close()
        # print(super().read_config(path=self.CONFIG.path, write=True))


if __name__ == '__main__':
    inst = Dev(
        config_path=CONFIG_PATH,
        config_contents=CONFIG.config_file,
        config_var='waf.secret_key',
    )
    # inst = Dev(name='somerawname', value='myvalue')
    inst.get_database_file()
    inst.open_database()
    inst.get_table()
    # print([i for i in inst.process()])
    # inst.insert_entries()
    print(inst.config_contents)
    print('here', inst.entry[inst.name])
    print(f'hash {inst.as_string(inst.hash)}')
    inst.get_all_items()
    print(inst.result)
    print(f'process = {inst.process()}')
    # inst.update_entries_in_config()
    # print(inst.config_path)
    # inst._open_config_file()

    # inst.get_all_items('Name="secret_key"')
    # print([i for i in inst.result[0]])
    # print(inst.database.get_items('netacea_slackbot', 'Name="somerawname"'))
    # print(inst.run_query('SELECT * FROM netacea_slackbot'))
    # print(inst.result)
    # inst.decode_items()
