from netacea_slackbot import CONFIG

from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute,
    UTCDateTimeAttribute,
    NumberAttribute,
)


class NetaceaSlackbotModel(Model):
    class Meta:
        table_name = 'netacea-slackbot'
        region = 'eu-west-1'
        write_capacity_units = 1
        read_capacity_units = 1
        aws_access_key_id = CONFIG.dynamodb_access_key
        aws_secret_access_key = CONFIG.dynamodb_secret_key
        aws_session_token = None

    team_name = UnicodeAttribute(hash_key=True)
    team_id = UnicodeAttribute()
    access_token = UnicodeAttribute()
    bot_access_token = UnicodeAttribute()
    bot_user_id = UnicodeAttribute()
    webhook_channel = UnicodeAttribute()
    webhook_channel_id = UnicodeAttribute()
    webhook_configuration_url = UnicodeAttribute()
    webhook_url = UnicodeAttribute()
    scope = UnicodeAttribute()
    user_id = UnicodeAttribute()
    date_installed = UTCDateTimeAttribute()


class DynamicWafModel(Model):
    class Meta:
        table_name = 'wh-ddos-dynamic-waf'
        aws_access_key_id = CONFIG.waf_access_key
        aws_secret_access_key = CONFIG.waf_secret_key
        aws_session_token = None

    NumericId = NumberAttribute(hash_key=True)
    ExpirationTime = NumberAttribute()
    IPCidr = UnicodeAttribute()
    Timestamp = NumberAttribute()
