from typing import TYPE_CHECKING

if TYPE_CHECKING:
    import pynamodb


def update_model_params(
    model: 'pynamodb.models.Model', **kwargs
) -> 'pynamodb.models.Model':
    for item in kwargs:
        setattr(model, item, kwargs[item])
    return model
