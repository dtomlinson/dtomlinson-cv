import json
from importlib import resources
from typing import Union
from ipaddress import IPv4Address

import pendulum

from netacea_slackbot import CONFIG
from netacea_slackbot.aws import sts
from netacea_slackbot import dynamodb
from netacea_slackbot.waf.exceptions import WhiteListError


class BlockIPAddressOnWaf:
    """docstring for BlockIPAddressOnWaf"""

    @property
    def __whitelist(self):
        res = resources.path('netacea_slackbot.waf', 'whitelist.json')
        with res as r:
            with open(r, 'r') as j:
                self._whitelist = json.load(j)
        return self._whitelist

    @property
    def ttl_measure(self):
        return self._ttl_measure

    @ttl_measure.setter
    def ttl_measure(self, measure: str):
        _ttl_measure = ['minutes', 'hours', 'days']
        if measure in _ttl_measure:
            self._ttl_measure = measure
        else:
            raise AttributeError(
                f'ttl_measure must be one of {_ttl_measure} not'
                f' {measure.__repr__()}'
            )

    @property
    def NumericId(self):
        return self._NumericId

    @NumericId.setter
    def NumericId(self, ip):
        if '/' not in ip:
            self.bareip = ip
            self.ip += '/32'
        else:
            self.bareip = ip.split('/')[0]
        self._NumericId = int(IPv4Address(self.bareip))

    @property
    def model_attributes(self):
        self._model_attributes = {
            'NumericId': self.NumericId,
            'ExpirationTime': self.ExpirationTime,
            'IPCidr': self.IPCidr,
            'Timestamp': self.Timestamp,
        }
        return self._model_attributes

    @property
    def ip(self):
        return self._ip

    @ip.setter
    def ip(self, ipaddress):
        if ipaddress in self.whitelist:
            raise WhiteListError(
                f'Cannot block {ipaddress.__repr__()} as it is in'
                ' the whitelist.'
            )
        else:
            self._ip = ipaddress

    def __init__(
        self,
        ip: str,
        *,
        ttl: int = 3,
        ttl_measure: str,
        proxy: Union[None, str] = None,
    ) -> None:

        self.whitelist = self.__whitelist
        self.ip = ip
        self.ttl = ttl
        self.ttl_measure = ttl_measure
        self.proxy = proxy
        self.NumericId = ip
        self.ExpirationTime = self._set_expiration_time()
        self.IPCidr = self.ip
        self.Timestamp = int(pendulum.now().timestamp())

        super().__init__()

    def _set_pendulum_dict(self):
        args = {self.ttl_measure: self.ttl}
        return args

    def _set_expiration_time(self):
        pendulum_dict = self._set_pendulum_dict()
        return int(pendulum.now().add(**pendulum_dict).timestamp())

    def _get_sts(self):
        credentials = sts.Boto3STSService(
            arn=CONFIG.waf_role_arn,
            aws_access_key_id=CONFIG.waf_access_key,
            aws_secret_access_key=CONFIG.waf_secret_key,
            region_name='eu-west-2',
            proxy=self.proxy,
        )
        # print(credentials.role_name)
        self.credentials = credentials.get_temporary_credentials()
        return self

    def _get_dynamo_model(self):
        self.dynamo_model = dynamodb.models.DynamicWafModel()
        return self

    def _update_dynamo_credentials(self):
        self.dynamo_model.Meta.aws_access_key_id = self.credentials[0]
        self.dynamo_model.Meta.aws_secret_access_key = self.credentials[1]
        self.dynamo_model.Meta.aws_session_token = self.credentials[2]
        return self

    def _update_dynamo_attributes(self):
        dynamodb.functionality.update_model_params(
            self.dynamo_model, **self.model_attributes
        )
        return self

    def update(self):
        self._get_dynamo_model()
        self._get_sts()
        self._update_dynamo_credentials()
        self._update_dynamo_attributes()
        # self.dynamo_model.save()


if __name__ == '__main__':
    inst = BlockIPAddressOnWaf('82.6.205.148', ttl=5, ttl_measure='minutes')
    # inst = BlockIPAddressOnWaf('87.121.79.55', ttl=5, ttl_measure='minutes')
    inst._get_sts()
    print(inst.credentials)
    print(inst.bareip)
    print(inst.ip)

    inst.update()

    print(f'inst.NumericId={inst.NumericId}')
    print(f'inst.ExpirationTime={inst.ExpirationTime}')
    print(f'inst.IPCidr={inst.IPCidr}')
    print(f'inst.Timestamp={inst.Timestamp}')
    # inst.update_dynamo_model()
    print(inst.dynamo_model.NumericId)
    print(inst.dynamo_model.ExpirationTime)
    print(inst.dynamo_model.IPCidr)
    print(inst.dynamo_model.Timestamp)
    print(inst.dynamo_model)
    # inst.dynamo_model.save()
