from __future__ import annotations
from abc import ABC, abstractmethod
from importlib import resources
import json
from pprint import pprint
import urllib.request
from urllib.error import HTTPError
import contextlib

import ipinfo

from netacea_slackbot import CONFIG


class UnblockIPConfirmBuilderConc(ABC):
    @property
    @abstractmethod
    def product(self) -> None:
        pass

    @abstractmethod
    def update_text(self, block_pos: int, value: str) -> dict:
        pass

    @abstractmethod
    def update_fields(
        self, block_pos: int, fields_pos: int, value: str
    ) -> dict:
        pass

    @abstractmethod
    def insert_name(self) -> None:
        pass

    @abstractmethod
    def insert_ip(self) -> None:
        pass

    @abstractmethod
    def insert_city(self) -> None:
        pass

    @abstractmethod
    def insert_isp(self) -> None:
        pass

    @abstractmethod
    def insert_region(self) -> None:
        pass

    @abstractmethod
    def insert_country(self) -> None:
        pass

    @abstractmethod
    def insert_timezone(self) -> None:
        pass


class UnblockIPConfirmBuilder(UnblockIPConfirmBuilderConc):
    def __init__(self) -> None:
        self.reset()

    def reset(self) -> None:
        self._product = UnblockIPConfirmBlock()

    @property
    def product(self) -> UnblockIPConfirmBlock:
        product = self._product
        self.reset()
        return product

    def update_text(self, block_pos: int, value: str) -> dict:
        d = self._product.response
        d['blocks'][block_pos]['text']['text'] = d['blocks'][block_pos][
            'text'
        ]['text'].replace('$VAR$', value)
        self._product.response.update(d)
        return d

    def update_fields(
        self, block_pos: int, fields_pos: int, value: str
    ) -> dict:
        d = self._product.response
        d['blocks'][block_pos]['fields'][fields_pos]['text'] = d['blocks'][
            block_pos
        ]['fields'][fields_pos]['text'].replace('$VAR$', value)
        self._product.response.update(d)
        return d

    def insert_name(self, slack_name: str):
        self.update_text(0, slack_name)
        return self

    def insert_ip(self, ip: str):
        self.update_fields(1, 0, ip)
        self._product.ip = ip
        return self

    def insert_city(self, city: str):
        self.update_fields(1, 1, city)
        self._product.city = city
        return self

    def insert_isp(self, isp: str):
        self.update_fields(1, 2, isp)
        self._product.isp = isp
        return self

    def insert_region(self, region: str):
        self.update_fields(1, 3, region)
        self._product.region = region
        return self

    def insert_country(self, country: str):
        self.update_fields(1, 4, country)
        self._product.country = country
        return self

    def insert_timezone(self, timezone: str):
        self.update_fields(1, 5, timezone)
        self._product.timezone = timezone
        return self


class UnblockIPConfirmBlock:
    """docstring for UnblockIPConfirmBlock"""

    def __init__(self) -> None:
        self.response = self.get_template()
        super().__init__()

    @property
    def ip(self) -> str:
        return self._ip

    @ip.setter
    def ip(self, ip: str) -> None:
        self._ip = ip

    @property
    def city(self) -> str:
        return self._city

    @city.setter
    def city(self, city: str) -> None:
        self._city = city

    @property
    def isp(self) -> str:
        return self._isp

    @isp.setter
    def isp(self, isp: str) -> None:
        self._isp = isp

    @property
    def region(self) -> str:
        return self._region

    @region.setter
    def region(self, region: str) -> None:
        self._region = region

    @property
    def country(self) -> str:
        return self._country

    @country.setter
    def country(self, country: str) -> None:
        self._country = country

    @property
    def timezone(self) -> str:
        return self._timezone

    @timezone.setter
    def timezone(self, timezone: str) -> None:
        self._timezone = timezone

    @staticmethod
    def get_template() -> dict:
        response_resource = resources.path(
            'netacea_slackbot.slackmessageblocks.templates.unblock',
            'response_0_unblock.json',
        )
        with response_resource as r:
            with open(r, 'r') as j:
                response_dict = json.load(j)
        return response_dict

    def get_response(self) -> dict:
        return self.response


class BuildUnblockIPResponse:
    """docstring for BuildUnblockIPResponse"""

    def __init__(self) -> None:
        self._builder = None
        super().__init__()

    @property
    def builder(self) -> UnblockIPConfirmBuilder:
        return self._builder

    @builder.setter
    def builder(self, builder: UnblockIPConfirmBuilder) -> None:
        self._builder = builder

    def _get_ip_info(self, ip: str) -> dict:
        ip_handler = ipinfo.getHandler(CONFIG.ipinfo_handler)
        return ip_handler.getDetails(ip).all

    def build_unblock_confirmation(self, name: str, ip: str):
        response = self._get_ip_info(ip)
        with contextlib.suppress(KeyError):
            self.builder.insert_name(name)
        with contextlib.suppress(KeyError):
            self.builder.insert_ip(ip)
        with contextlib.suppress(KeyError):
            self.builder.insert_city(response['city'])
        with contextlib.suppress(KeyError):
            self.builder.insert_isp(response['org'])
        with contextlib.suppress(KeyError):
            self.builder.insert_region(response['region'])
        with contextlib.suppress(KeyError):
            self.builder.insert_country(response['country'])
        with contextlib.suppress(KeyError):
            self.builder.insert_timezone(response['timezone'])

    def build_unblock_confirmation_manual(
        self,
        name: str = None,
        ip: str = None,
        city: str = None,
        isp: str = None,
        region: str = None,
        country: str = None,
        timezone: str = None,
    ):
        self.builder.insert_name(name)
        self.builder.insert_ip(ip)
        self.builder.insert_city(city)
        self.builder.insert_isp(isp)
        self.builder.insert_region(region)
        self.builder.insert_country(country)
        self.builder.insert_timezone(timezone)


if __name__ == '__main__':
    director = BuildUnblockIPResponse()
    builder = UnblockIPConfirmBuilder()
    director.builder = builder
    director.build_unblock_confirmation(
        name='daniel', ip='148.219.236.233',
    )
    product = builder.product.response
    pprint(type(product))
    pprint(product)
    product = builder.product.response
    pprint(product)
    # pprint(product.response)
    # print(product.ip)
    # print(product.isp)
    # product = builder.product.get_response()
    # pprint(product)
