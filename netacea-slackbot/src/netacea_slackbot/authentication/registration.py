from typing import TYPE_CHECKING, Tuple

if TYPE_CHECKING:
    from slack.web.base_client import SlackResponse


def get_user_info(
    slack_response: 'SlackResponse',
) -> Tuple[str, str, str, str, str, str, str, str, str, str, str]:
    access_token = slack_response.data['access_token']
    bot_access_token = slack_response.data['bot']['bot_access_token']
    bot_user_id = slack_response.data['bot']['bot_user_id']
    webhook_channel = slack_response.data['incoming_webhook']['channel']
    webhook_channel_id = slack_response.data['incoming_webhook']['channel_id']
    webhook_configuration_url = slack_response.data['incoming_webhook'][
        'configuration_url'
    ]
    webhook_url = slack_response.data['incoming_webhook']['url']
    scope = slack_response.data['scope']
    team_id = slack_response.data['team_id']
    team_name = slack_response.data['team_name']
    user_id = slack_response.data['user_id']
    return (
        access_token,
        bot_access_token,
        bot_user_id,
        webhook_channel,
        webhook_channel_id,
        webhook_configuration_url,
        webhook_url,
        scope,
        team_id,
        team_name,
        user_id,
    )
