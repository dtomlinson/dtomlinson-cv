Each factory will create different products, but only products of the same variant. This means your factory should have a method to create (instantiate) each product. 

Each one of these products needs creating abstractly, and you create variants from these products. (It's swapped!)

E.g a Yes factory creates level 0 and level 1 responses.

Level 0 is an abstract product and level 1 is an abstract product

You create a yes level 0 product and a yes level 1 product.

These two products are seperate (the level is the abstract class) but they are of the same variant - so the factory can create them.

You should define your code so that different variants can cooperate with each other. So even though they are different products, the fact they are created under one factory which groups variants together ensures they are compatible!


Abstract factory - declares methods that returns different abstract products

Concrete factory - this is what the client uses to do things. This factory should provide methods to return all instances of each product. This way, the client can use this factory with the client code to create products.

Abstract products - Each product is a reaction to a value (e.g a yes or a no). These products (no matter their level) should always have a function to return a response (which is declared abstractly). Any additional methods to create this response go in the concrete product itself.

Concrete product - The implementation of the response. This means that all products (for a yes for example) will have the same method to return a response. This guarantees that no matter the level, a yes command will always have this method defined to send a response.

Variants of products should be able to work together!!! - this is key to the idea. If the variants can't work together, you shouldn't use this.

I.e in this case, either Level 0 responses should be able to work together (if level is how you split each product by) or Yes responses should be able to work together (if you split each product by Yes or Cancel)

Doing 

class YesResponseLevel0(YesResponse):
    """docstring for YesResponseLevel0"""

    def send_response(self):
        pass


class YesResponseLevel1(YesResponse):
    """docstring for YesResponseLevel1"""

    def send_response(self):
        pass


class CancelResponseLevel0(CancelResponse):
    """docstring for CancelResponseLevel0"""

    def send_response(self):
        pass


class CancelResponseLevel1(CancelResponse):
    """docstring for CancelResponseLevel0"""

    def send_response(self):
        pass

Is an example of splitting each product by level. Each level of product, e.g. YesResponseLevel1 and CancelResponseLevel1 should work together - even though they are different products they are still the same variant and should be compatible.
