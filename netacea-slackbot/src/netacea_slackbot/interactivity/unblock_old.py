from abc import ABC, abstractmethod, abstractstaticmethod

import requests

from netacea_slackbot import functionality


class InteractivityResponse(ABC):
    """docstring for InteractivityResponse"""

    @abstractmethod
    def __init__(self) -> None:
        super().__init__()

    @abstractstaticmethod
    def get_values(action_id: str, action_value: str):
        pass

    @abstractmethod
    def _load_response(self, response_type: str) -> dict:
        pass

    @abstractmethod
    def send_response(self, response_type: str, response_url: str) -> None:
        pass


class UnblockInteractivityLevel0(InteractivityResponse):
    """docstring for UnblockInteractivity"""

    responses: dict = {
        'yes': 'interactivity_0_yes.json',
        'no': 'interactivity_0_no.json',
        'timeout': 'interactivity_timeout.json',
    }

    def __init__(self):
        super().__init__()

    @staticmethod
    def get_values(action_id: str, action_value: str):
        pass

    def _load_response(self, response_type: str) -> dict:
        response = functionality.load_response(self.responses[response_type])
        return response

    def send_response(self, response_type: str, response_url: str) -> None:
        response = self._load_response(response_type)
        requests.post(response_url, json=response)

    def test(self):
        print('test')


# class UnblockInteractivityLevel1(UnblockInteractivityLevel0):
#     """docstring for UnblockInteractivityLevel1"""

#     def __init__(self):
#         super().__init__()

if __name__ == '__main__':
    inst = UnblockInteractivityLevel0().test()
