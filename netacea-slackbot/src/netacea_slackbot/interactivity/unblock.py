from __future__ import annotations
from abc import ABC, abstractmethod

from netacea_slackbot.mixins import LoadResponseMixin, SendResponseMixin
from netacea_slackbot.functionality import get_interactivity_params


class InteractivityResponse(ABC):
    """docstring for InteractivityResponse"""

    @abstractmethod
    def create_yes_response(self) -> YesResponse:
        pass

    @abstractmethod
    def create_cancel_response(self) -> CancelResponse:
        pass


class Level1ResponseFactory(InteractivityResponse):
    """docstring for Level1ResponseFactory"""

    def create_yes_response(self) -> YesResponseLevel1:
        return YesResponseLevel1()

    def create_cancel_response(self) -> CancelResponseLevel1:
        return CancelResponseLevel1()


class Level2ResponseFactory(InteractivityResponse):
    """docstring for Level1ResponseFactory"""

    def create_yes_response(self) -> YesResponseLevel2:
        return YesResponseLevel2()

    def create_cancel_response(self) -> CancelResponseLevel2:
        return CancelResponseLevel2()


class YesResponse(ABC):
    """docstring for YesResponse"""

    @property
    @abstractmethod
    def response(self) -> dict:
        pass

    @property
    @abstractmethod
    def confirmation(self) -> dict:
        pass

    @abstractmethod
    def action(self) -> None:
        pass


class CancelResponse(ABC):
    """docstring for CancelResponse"""

    @property
    @abstractmethod
    def response(self) -> dict:
        pass

    @property
    @abstractmethod
    def confirmation(self) -> dict:
        pass

    @abstractmethod
    def action(self) -> bool:
        pass


class YesResponseLevel1(YesResponse, LoadResponseMixin):
    """docstring for YesResponseLevel1"""

    @property
    def response(self) -> dict:
        resp = super().load_response(
            cmd_name='unblock',
            cmd_type='response',
            cmd_level='1',
            cmd_value='yes',
        )
        self.result = self.action()
        return resp

    @property
    def confirmation(self) -> dict:
        conf = super().load_response(
            cmd_name='unblock',
            cmd_type='confirmation',
            cmd_level='1',
            cmd_value='yes',
        )
        if self.result:
            return conf
        else:
            return {'message': 'some generic error'}

    def action(self) -> bool:
        # If action was successful
        if True:
            print('do something yes level 1')
            return True
        # If action was not successful
        else:
            return False


class CancelResponseLevel1(CancelResponse, LoadResponseMixin):
    """docstring for CancelResponseLevel1"""

    @property
    def response(self) -> None:
        resp = super().load_response(
            cmd_name='unblock',
            cmd_type='response',
            cmd_level='1',
            cmd_value='cancel',
        )
        self.action()
        return resp

    @property
    def confirmation(self) -> dict:
        conf = super().load_response(
            cmd_name='unblock',
            cmd_type='confirmation',
            cmd_level='1',
            cmd_value='cancel',
        )
        return conf

    def action(self) -> bool:
        print('do something')
        return True


class YesResponseLevel2(YesResponse, LoadResponseMixin):
    """docstring for YesResponseLevel2"""

    @property
    def response(self) -> dict:
        resp = super().load_response(
            cmd_name='unblock',
            cmd_type='response',
            cmd_level='2',
            cmd_value='yes',
        )
        self.action()
        return resp

    @property
    def confirmation(self) -> dict:
        conf = super().load_response(
            cmd_name='unblock',
            cmd_type='confirmation',
            cmd_level='2',
            cmd_value='yes',
        )
        return conf

    def action(self) -> bool:
        print('do something')
        return True


class CancelResponseLevel2(CancelResponse, LoadResponseMixin):
    """docstring for CancelResponseLevel2"""

    @property
    def response(self) -> dict:
        resp = super().load_response(
            cmd_name='unblock',
            cmd_type='response',
            cmd_level='2',
            cmd_value='cancel',
        )
        self.action()
        return resp

    @property
    def confirmation(self) -> dict:
        conf = super().load_response(
            cmd_name='unblock',
            cmd_type='confirmation',
            cmd_level='2',
            cmd_value='cancel',
        )
        return conf

    def action(self) -> bool:
        print('do something')
        return True


class Unblock(SendResponseMixin):
    """docstring for Unblock"""

    @property
    def factories(self):
        _factories: dict = {
            '1': Level1ResponseFactory(),
            '2': Level2ResponseFactory(),
        }
        return _factories

    @property
    def factory_methods(self):
        _factory_methods: dict = {
            'yes': self._yes_response,
            'cancel': self._cancel_response,
        }
        return _factory_methods

    def _yes_response(self):
        resp = self.factory.create_yes_response()
        return resp.response, resp.confirmation

    def _cancel_response(self):
        resp = self.factory.create_cancel_response()
        return resp.response, resp.confirmation

    def make_responses(self, action_id: str):
        cmd_name, cmd_type, cmd_level, cmd_value = get_interactivity_params(
            action_id
        )
        print(cmd_name, cmd_type, cmd_level, cmd_value)
        self.factory = self.factories[cmd_level]
        self.factory_method = self.factory_methods[cmd_value]
        self.result = self.factory_method()
        return self

    def send_response(
        self, response_url: str, response: dict, in_channel: bool = False
    ):
        super().send_response(
            response_url=response_url, response=response, in_channel=in_channel
        )
        return self


if __name__ == '__main__':
    # YesResponseLevel1()
    action_id = 'unblock_response_1_yes'
    inst = Unblock().make_responses(action_id)
    # for i in [i for i in inst.result]:
    #     print('\n', i)
    inst.send_response()
