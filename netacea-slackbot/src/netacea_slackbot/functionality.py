from importlib import resources
import json
from typing import Tuple
import base64


def load_response(cmd_name: str, cmd_level: str, cmd_value) -> dict:
    resp = resources.path(
        f'netacea_slackbot.slackmessageblocks.templates.{cmd_name}',
        f'{cmd_level}_{cmd_value}.json'
    )
    with resp as r:
        with open(r, 'r') as j:
            response = json.load(j)

    return response


def get_interactivity_params(action_id: str) -> Tuple[str, str, str, str]:
    """Utility function which takes an action_id from a Slack interactivity
    command and returns each component.

    Parameters
    ----------
    action_id : str
        The action_id which is returned from an interactivty command.

    Returns
    -------
    Tuple[str, str, str, str]
        Tuple containing the following components of the action_id:

        `cmd_name, cmd_type, cmd_level, cmd_value`

    cmd_name: str
        The name of the command. Example: unblock.

    cmd_type: str
        The type of the command. Example: response.

    cmd_level: str
        The level of the command. The first response would be level 1,
        a response in response to this would be level 2.

    cmd_value: str
        The value of the command. Example: yes.

    """
    cmd_name, cmd_type, cmd_level, cmd_value = action_id.split('_')
    return (cmd_name, cmd_type, cmd_level, cmd_value)


def encode_bytes_for_task(b_string: bytes):
    # Encode the bytes object as a bytes ascii object
    bytes_ascii = base64.encodebytes(b_string)
    # Decode the object as a UTF string
    bytes_string = bytes_ascii.decode('ascii')
    return bytes_string


def decode_bytes_for_task(b_string: str):
    # Encode the UTF string as a bytes ascii object
    bytes_string = b_string.encode('ascii')
    # Decode the ascii bytes into a bytes object
    bytes_ascii = base64.decodebytes(bytes_string)
    return bytes_ascii
