from typing import Tuple
import requests

from zappa.asynchronous import task

from netacea_slackbot.verification import VerifyValidIP
from netacea_slackbot.builder import (
    BuildUnblockIPResponse,
    UnblockIPConfirmBuilder,
)
from netacea_slackbot.functionality import decode_bytes_for_task


class Unblock():
    """docstring for Unblock"""

    def __init__(self, body: bytes, request_form: dict) -> None:
        self.body = body
        self.request_form = request_form
        self.params = self._get_response_params()
        super().__init__()

    def _get_response_params(self) -> Tuple[str, str, str]:
        name = f'@{self.request_form["user_id"]}'
        response_url = self.request_form['response_url']
        ip = VerifyValidIP(self.request_form['text']).get_ip()
        return (name, ip, response_url)

    def _build_response(self) -> dict:
        director = BuildUnblockIPResponse()
        builder = UnblockIPConfirmBuilder()
        director.builder = builder
        director.build_unblock_confirmation(
            name=self.params[0], ip=self.params[1]
        )
        return builder.product.response

    def send_response(self, in_channel: bool = False):
        response = self._build_response()
        if in_channel:
            response.update({"response_type": "in_channel"})
        requests.post(url=self.params[2], json=response)


@task
def send_response_unblock_zappa(body: str, request_form: dict):
    body = decode_bytes_for_task(body)
    unblock_response = Unblock(
        body=body, request_form=request_form
    )
    unblock_response.send_response(in_channel=True)
