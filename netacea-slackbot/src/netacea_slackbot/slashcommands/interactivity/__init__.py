from typing import Union, Tuple

from .unblock import Unblock
from netacea_slackbot.functionality import get_interactivity_params


class ProcessInteraction:
    """docstring for ProcessInteraction"""

    @property
    def slash_commands(self) -> dict:
        _slash_commands = {'unblock': Unblock}
        return _slash_commands

    def __init__(
        self,
        action_id: str,
        response_url: str,
        in_channel: bool = False,
    ) -> None:

        self.in_channel = in_channel
        self.action_id = action_id
        self.response_url = response_url
        (
            self.cmd_name,
            self.cmd_type,
            self.cmd_level,
            self.cmd_value,
        ) = get_interactivity_params(action_id)

    def _get_command(self) -> Union[Unblock]:
        command = self.slash_commands[self.cmd_name]
        return command

    def process(self):
        command = self._get_command()
        command(
            response_url=self.response_url,
            in_channel=self.in_channel,
        ).handle_interactivity(self.action_id)
