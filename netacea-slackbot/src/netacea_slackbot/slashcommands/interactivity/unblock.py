from __future__ import annotations
from abc import ABC, abstractmethod
from time import sleep

from zappa.asynchronous import task

from netacea_slackbot.mixins import LoadResponseMixin, SendResponseMixin
from netacea_slackbot.functionality import get_interactivity_params


class InteractivityResponse(ABC):
    """docstring for InteractivityResponse"""

    @abstractmethod
    def create_yes_response(self) -> YesResponse:
        pass

    @abstractmethod
    def create_cancel_response(self) -> CancelResponse:
        pass


class Level1ResponseFactory(InteractivityResponse):
    """docstring for Level1ResponseFactory"""

    def create_yes_response(self) -> YesResponseLevel1:
        return YesResponseLevel1()

    def create_cancel_response(self) -> CancelResponseLevel1:
        return CancelResponseLevel1()


class Level2ResponseFactory(InteractivityResponse):
    """docstring for Level1ResponseFactory"""

    def create_yes_response(self) -> YesResponseLevel2:
        return YesResponseLevel2()

    def create_cancel_response(self) -> CancelResponseLevel2:
        return CancelResponseLevel2()


class YesResponse(ABC):
    """docstring for YesResponse"""

    @property
    @abstractmethod
    def initial_response(self) -> dict:
        pass

    @property
    @abstractmethod
    def confirmation_response(self) -> dict:
        pass

    @abstractmethod
    def action(self) -> None:
        pass


class CancelResponse(ABC):
    """docstring for CancelResponse"""

    @property
    @abstractmethod
    def initial_response(self) -> dict:
        pass

    @property
    @abstractmethod
    def confirmation_response(self) -> dict:
        pass

    @abstractmethod
    def action(self) -> bool:
        pass


class YesResponseLevel1(LoadResponseMixin, YesResponse):
    """docstring for YesResponseLevel1"""

    @property
    def initial_response(self) -> dict:
        resp = super().load_response(
            cmd_name='unblock',
            cmd_type='response',
            cmd_level='1',
            cmd_value='yes',
        )
        return resp

    @property
    def confirmation_response(self) -> dict:
        conf_success = super().load_response(
            cmd_name='unblock',
            cmd_type='confirmation',
            cmd_level='1',
            cmd_value='yes',
        )
        conf_error = super().load_response(
            cmd_name='unblock',
            cmd_type='error',
            cmd_level='1',
            cmd_value='yes',
        )

        self.result = self.action()

        if self.result:
            return conf_success
        else:
            return conf_error

    def action(self) -> bool:
        # If action was successful
        if True:
            print('do something yes level 1')
            sleep(2)
            return True
        # If action was not successful
        else:
            return False


class CancelResponseLevel1(LoadResponseMixin, CancelResponse):
    """docstring for CancelResponseLevel1"""

    @property
    def initial_response(self) -> None:
        resp = super().load_response(
            cmd_name='unblock',
            cmd_type='response',
            cmd_level='1',
            cmd_value='cancel',
        )
        return resp

    @property
    def confirmation_response(self) -> dict:
        conf_success = super().load_response(
            cmd_name='unblock',
            cmd_type='confirmation',
            cmd_level='1',
            cmd_value='cancel',
        )
        conf_error = super().load_response(
            cmd_name='unblock',
            cmd_type='error',
            cmd_level='1',
            cmd_value='cancel',
        )

        self.result = self.action()

        if self.result:
            return conf_success
        else:
            return conf_error

    def action(self) -> bool:
        print('cancelling...')
        return True


class YesResponseLevel2(LoadResponseMixin, YesResponse):
    """docstring for YesResponseLevel2"""

    @property
    def initial_response(self) -> dict:
        resp = super().load_response(
            cmd_name='unblock',
            cmd_type='response',
            cmd_level='2',
            cmd_value='yes',
        )
        return resp

    @property
    def confirmation_response(self) -> dict:
        conf_success = super().load_response(
            cmd_name='unblock',
            cmd_type='confirmation',
            cmd_level='2',
            cmd_value='yes',
        )
        conf_error = super().load_response(
            cmd_name='unblock',
            cmd_type='error',
            cmd_level='2',
            cmd_value='yes',
        )

        self.result = self.action()

        if self.result:
            return conf_success
        else:
            return conf_error

    def action(self) -> bool:
        # If action was successful
        if True:
            print('do something yes level 2')
            return True
        # If action was not successful
        else:
            return False


class CancelResponseLevel2(LoadResponseMixin, CancelResponse):
    """docstring for CancelResponseLevel2"""

    @property
    def initial_response(self) -> None:
        resp = super().load_response(
            cmd_name='unblock',
            cmd_type='response',
            cmd_level='2',
            cmd_value='cancel',
        )
        return resp

    @property
    def confirmation_response(self) -> dict:
        conf_success = super().load_response(
            cmd_name='unblock',
            cmd_type='confirmation',
            cmd_level='2',
            cmd_value='cancel',
        )
        conf_error = super().load_response(
            cmd_name='unblock',
            cmd_type='error',
            cmd_level='2',
            cmd_value='cancel',
        )

        self.result = self.action()

        if self.result:
            return conf_success
        else:
            return conf_error

    def action(self) -> bool:
        print('cancelling...')
        return True


class SlashCommandInteractivity(ABC):
    """docstring for SlashCommandInteractivity"""

    @property
    @abstractmethod
    def factory_methods(self) -> dict:
        pass

    @property
    @abstractmethod
    def factories(self) -> dict:
        pass

    @abstractmethod
    def __init__(self) -> None:
        pass

    @abstractmethod
    def handle_interactivity(self) -> None:
        pass

    @abstractmethod
    def send_response(self) -> None:
        pass


class Unblock(SendResponseMixin, SlashCommandInteractivity):
    """docstring for Unblock"""

    @property
    def factories(self):
        _factories: dict = {
            '1': Level1ResponseFactory(),
            '2': Level2ResponseFactory(),
        }
        return _factories

    @property
    def factory_methods(self):
        _factory_methods: dict = {
            'yes': self._yes_response,
            'cancel': self._cancel_response,
        }
        return _factory_methods

    def __init__(self, response_url: str, in_channel: bool):
        self.response_url = response_url
        self.in_channel = in_channel

    def _yes_response(self):
        self.resp = self.factory.create_yes_response()
        return self

    def _cancel_response(self):
        self.resp = self.factory.create_cancel_response()
        return self

    def handle_interactivity(self, action_id: str) -> None:
        cmd_name, cmd_type, cmd_level, cmd_value = get_interactivity_params(
            action_id
        )

        # Get the abstract factory for the slash command
        self.factory = self.factories[cmd_level]

        # Get the abstract factory method for the slash command
        self.factory_method = self.factory_methods[cmd_value]

        # Run the abstract factory method for the slash command
        self.result = self.factory_method()

        # Get the initial response before the action is done
        initial_response = self.result.resp.initial_response

        # Send the initial response to the user
        self.send_response(initial_response)

        print(initial_response)

        # Run the action and get the confirmation response
        confirmation_response = self.result.resp.confirmation_response

        # Send the confirmation response to the user
        self.send_response(confirmation_response)

        print(confirmation_response)

    def send_response(self, response: dict):
        super().send_response(
            response_url=self.response_url,
            response=response,
            in_channel=self.in_channel,
        )
        return self


class Timeout(SendResponseMixin, LoadResponseMixin, SlashCommandInteractivity):
    """docstring for Timeout"""

    @property
    def factories(self):
        return None

    @property
    def factory_methods(self):
        return None

    def __init__(self, response_url: str, in_channel: bool) -> None:
        self.response_url = response_url
        self.in_channel = in_channel

    def handle_interactivity(self):
        self.response = super().load_response(
            cmd_name='generic',
            cmd_type='error',
            cmd_level='0',
            cmd_value='timeout',
        )
        return self

    def send_response(self) -> None:
        self.handle_interactivity()
        super().send_response(
            response_url=self.response_url,
            response=self.response,
            in_channel=self.in_channel
        )
        return self
