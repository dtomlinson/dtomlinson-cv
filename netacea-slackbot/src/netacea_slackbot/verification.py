from dataclasses import dataclass, field
from typing import TYPE_CHECKING, Union, Any
import hmac
import hashlib
import base64
import re
import contextlib

import pendulum

from netacea_slackbot import CONFIG
from netacea_slackbot.regex.regex import ip_regex


@dataclass
class VerifyTimeStamp:
    """docstring for VerifyTimeStamp"""

    slack_ts: int
    _slack_ts: int = field(init=False, repr=False)

    @property
    def slack_ts(self) -> int:
        return self._slack_ts

    @slack_ts.setter
    def slack_ts(self, slack_ts: int):
        self._slack_ts = pendulum.from_timestamp(slack_ts)

    @staticmethod
    def _check_time_type(timestamp: Union[str, int]):
        if isinstance(timestamp, str):
            timestamp = int(timestamp)
        elif not isinstance(timestamp, int):
            raise TypeError(
                f'timestamp should be str or int not'
                f' {timestamp.__class__}'
            )
        return timestamp

    @staticmethod
    def _normalise_epoch(epoch: int):
        with contextlib.suppress(ValueError):
            epoch_time, epoch_time_d = str(epoch).split('.')
            print(epoch_time)
            return epoch_time
        print(epoch)
        return epoch

    @staticmethod
    def _convert_to_pendulum(timestamp: Union[str, int]):
        timestamp = VerifyTimeStamp._normalise_epoch(timestamp)
        timestamp = VerifyTimeStamp._check_time_type(timestamp)
        return pendulum.from_timestamp(timestamp)

    @staticmethod
    def verify_two_timestamps(
        timestamp_one: Union[str, int],
        timestamp_two: Union[str, int],
        minutes: int,
    ):
        timestamp_one = VerifyTimeStamp._convert_to_pendulum(timestamp_one)
        timestamp_two = VerifyTimeStamp._convert_to_pendulum(timestamp_two)
        diff = timestamp_two.diff(timestamp_one).in_minutes()
        if diff <= minutes:
            return True
        else:
            return False

    def verify_timestamp(self):
        self.time_window = pendulum.now().subtract(minutes=5)
        diff = self._slack_ts.diff(self.time_window).in_minutes()
        if diff <= 5:
            return True
        else:
            return False


@dataclass
class VerifySlackResponse:
    """docstring for VerifySlackResponse"""

    slack_sig: str
    slack_ts: int
    body: bytes
    signing_secret: str = bytes(CONFIG.slack_signing_secret, 'utf-8')

    _slack_ts: int = field(init=False, repr=False)
    _body: bytes = field(init=False, repr=False)

    @property
    def slack_ts(self) -> str:
        return self._slack_ts

    @slack_ts.setter
    def slack_ts(self, slack_ts: int):
        self._slack_ts = str(slack_ts)

    @property
    def body(self) -> str:
        return self._body

    @body.setter
    def body(self, body: bytes) -> str:
        self._body = body.decode('utf-8')

    def validate_response(self) -> bool:
        self.base_string = f'v0:{self.slack_ts}:{self.body}'.encode('utf-8')

        digest = (
            'v0='
            + hmac.new(
                self.signing_secret, self.base_string, digestmod=hashlib.sha256
            ).hexdigest()
        )

        if hmac.compare_digest(digest, self.slack_sig):
            return True
        else:
            return False


class VerifyValidIP:
    """docstring for VerifyValidIP"""

    def __init__(self, string_to_verify: str) -> None:
        self.string_to_verify = string_to_verify

    def _match_ip(self) -> Union[None, str]:
        ip = None
        match = re.match(ip_regex, self.string_to_verify)
        with contextlib.suppress(AttributeError):
            ip = match.group()
        return ip

    def verify_ip(self) -> bool:
        ip = self._match_ip()
        if ip is not None:
            return True
        else:
            return False

    def get_ip(self) -> Union[None, str]:
        ip = self._match_ip()
        return ip


class VerifyUser:
    """docstring for VerifyUser"""

    def __init__(self, user: str) -> None:
        self.user = user
        self.authorised_users = None

    def verify_user(self):
        with contextlib.suppress(AttributeError):
            self.authorised_users = CONFIG.authorised_users
        try:
            if (
                self.user in self.authorised_users
                and self.authorised_users is not None
            ):
                print('auth')
                return True
            elif (
                self.user not in self.authorised_users
                and self.authorised_users is not None
            ):
                print('not auth')
                return False
        except TypeError:
            print('except')
            if self.authorised_users is None:
                return True


if __name__ == '__main__':
    # resp = VerifyUser('UR2PDTER1').verify_user()
    # print(resp)
    ts = VerifyTimeStamp.verify_two_timestamps(
        1576506584, 1576506230.1576506584, minutes=5
    )
    print(ts)
