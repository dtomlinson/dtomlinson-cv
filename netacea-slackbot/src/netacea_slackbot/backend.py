import requests
from pprint import pprint
import json

# import threading
from urllib.error import HTTPError
import typing
from typing import Tuple
from datetime import datetime

from flask import Flask, request, abort
import slack
from zappa.asynchronous import task

from netacea_slackbot import CONFIG
from netacea_slackbot import verification
from netacea_slackbot.slashcommands.unblock import send_response_unblock_zappa
import netacea_slackbot.slashcommands.interactivity as interactivity
from netacea_slackbot.interactivity.unblock import Unblock as UnblockResponse
from netacea_slackbot.builder import (
    BuildUnblockIPResponse,
    UnblockIPConfirmBuilder,
)
from netacea_slackbot.functionality import encode_bytes_for_task
from netacea_slackbot.authentication.registration import get_user_info
from netacea_slackbot.aws import sts
from netacea_slackbot import dynamodb

if typing.TYPE_CHECKING:
    import werkzeug.local.LocalProxy


app = Flask(__name__)


def get_request_params(
    request: 'werkzeug.local.LocalProxy',
) -> Tuple[str, str, str, str, str]:
    """Function to get the most common request parameters from a slash command.


    Parameters
    ----------
    request : werkzeug.local.LocalProxy
        The request object from the command.

    Returns
    -------
    Tuple[str, str, str, str, str]
        Tuple containing the following paramters:

        `body, headers, request_form, slack_ts, slack_sig`

    body: bytes (index 0)
        Raw body of the request.
    headers: dict (index 1)
        Dict containing all the headers sent with the request.the
    request_form: dict (index 2)
        Dict containing all key:values of the body.
    slack_ts: int (index 3)
        Timestamp from slack.
    slack_sig: str (index 4)
        Slack signing signature. This is used to verify the request came
        from Slack.
    """
    body = request.get_data()
    headers = request.headers
    request_form = request.form.to_dict()
    slack_ts = int(headers.get('X-Slack-Request-Timestamp'))
    slack_sig = str(headers.get('X-Slack-Signature'))

    return (body, headers, request_form, slack_ts, slack_sig)


def get_payload_params(payload: dict) -> Tuple[str, str, str, str, str, str]:
    """Function to get the most common payload parameters from an interactive
    component.

    Parameters
    ----------
    payload : dict
        The payload from the interactive request

    Returns
    -------
    Tuple[str, str, str, str, str, str]
        Tuple containing the following parameters:

            `action_id, action_ts, action_value, message_ts, response_url,`
            `action_user`

    action_id: str
        The action ID from the command that the interactive request came
        from (index 0)
    action_ts: str
        The timestamp from the action (index 1)
    action_value: str
        The value token from the interactactive request (index 2)
    message_ts: str
        The timestamp of the original message (index 3)
    response_url: str
        The response URL to reply directly to the interactive component
        that the user is viewing (index 4)
    action_user: str
        The user ID that sent the interactive request (index 5)
    """
    action_id = payload['actions'][0]['action_id']
    action_ts = payload['actions'][0]['action_ts']
    action_value = payload['actions'][0]['value']
    message_ts = payload['message']['ts']
    response_url = payload['response_url']
    action_user = payload['user']['id']
    return (
        action_id,
        action_ts,
        action_value,
        message_ts,
        response_url,
        action_user,
    )


def get_payload(payload: str):
    payload = json.loads(payload)
    return payload


@task
def test_0(response_url: str):
    requests.post(
        response_url,
        json=json.loads(
            '{"text": "hello_test_0", "response_type": "ephemeral"}'
        ),
    )


@app.route('/interactive', methods=['GET', 'POST'])
def interactive() -> None:

    # Get request params
    (body, headers, request_form, slack_ts, slack_sig) = get_request_params(
        request
    )

    # Get payload
    payload = get_payload(request_form['payload'])

    # Get payload params
    (
        action_id,
        action_ts,
        action_value,
        message_ts,
        response_url,
        action_user,
    ) = get_payload_params(payload)

    # Verify response came from Slack
    verify_response = verification.VerifySlackResponse(
        slack_sig=slack_sig, slack_ts=slack_ts, body=body
    ).validate_response()

    # Verify response came within 5 minute window
    verify_time = verification.VerifyTimeStamp.verify_two_timestamps(
        timestamp_one=action_ts, timestamp_two=message_ts, minutes=5
    )

    # Verify user is authorised to run the command
    verify_user = verification.VerifyUser(action_user).verify_user()

    # Do the interativity
    if verify_response and verify_time and verify_user:
        interactivity.ProcessInteraction(
            action_id=action_id, response_url=response_url, in_channel=True
        ).process()
    elif not verify_user:
        return "Oops! You're not authorised to send this command!", 200
    elif not verify_time:
        interactivity.unblock.Timeout(
            response_url=response_url, in_channel=True
        ).send_response()
    else:
        abort(400)

    return '', 200


@app.route('/hello-world-old', methods=['GET', 'POST'])
def hello_world():
    body = request.get_data()
    pprint(type(body))
    pprint(type(request.form))
    print(typing.get_type_hints(request.form))
    response_url = request.form['response_url']
    print('IP = ', request.form['text'])
    print(f'request.form={request.form}')
    # print(f'request.form={request.form["response_url"]}')
    # pprint(f'request.headers={request.headers}')
    # print(CONFIG.slack_signing_secret)

    # pprint(request.get_data(parse_form_data=False))
    # response = {'response_type': 'in_channel', 'text': 'Hello, world!'}
    # requests.post(
    #     request.form['response_url'], json=json.loads(resp, strict=False)
    # )
    # test_0(response_url=response_url)
    # t = threading.Thread(target=get_ip)
    # t = threading.Thread(target=test_1, args=(response_url,))
    # t.start()

    # build the response
    director = BuildUnblockIPResponse()
    builder = UnblockIPConfirmBuilder()
    director.builder = builder
    director.build_unblock_confirmation(
        name=f'@{request.form["user_id"]}', ip=request.form['text']
    )
    product = builder.product
    # print(f'type:{type(request.form["response_url"])}')
    requests.post(response_url, json=product.response)
    return '', 200


# Using threading
# @app.route('/hello-world', methods=['POST'])
# def unblock_ip():
#     body = request.get_data()
#     request_form = request.form.to_dict()
#     unblock_response = Unblock(body=body, request_form=request_form)
#     t = threading.Thread(target=unblock_response.send_response)
#     t.start()
#     return '', 200


@app.route('/unblock', methods=['POST'])
def unblock_ip():

    # Get request params
    (body, headers, request_form, slack_ts, slack_sig) = get_request_params(
        request
    )

    # Verify timestamp within 5 minutes
    verify_time = verification.VerifyTimeStamp(
        slack_ts=slack_ts
    ).verify_timestamp()

    # Verify response came from Slack
    verify_response = verification.VerifySlackResponse(
        slack_sig=slack_sig, slack_ts=slack_ts, body=body
    ).validate_response()

    # Verify argument is a valid IP
    verify_ip = verification.VerifyValidIP(request_form['text']).verify_ip()
    if not verify_ip:
        return (
            f"Oops! {request.form['text']} does not appear to be a valid IP!",
            200,
        )

    # Verify user is authorised to run the command
    verify_user = verification.VerifyUser(
        request_form['user_id']
    ).verify_user()

    # Process request
    if verify_time and verify_response and verify_user:
        try:
            body = encode_bytes_for_task(body)
            send_response_unblock_zappa(body=body, request_form=request_form)
        except HTTPError:
            return (
                f"Oops! {request.form['text']} appears to be a valid IP but",
                f" no information could be found - is this a real IP?",
                200,
            )
    elif not verify_user:
        return "Oops! You're not authorised to send this command!", 200
    elif not verify_time:
        interactivity.unblock.Timeout(
            response_url=request_form['response_url'], in_channel=True
        ).send_response()
    else:
        abort(400)
    return '', 200


@app.route('/authenticate', methods=['GET', 'POST'])
def authenticate_with_slack():

    # Retreive the authentication code from the request parameters
    auth_code = request.args['code']

    # Create a webclient to authenticate with Slack
    client = slack.WebClient(token="")

    # Request the authentication tokens from Slack
    response = client.oauth_access(
        client_id=CONFIG.slack_client_id,
        client_secret=CONFIG.slack_client_secret,
        code=auth_code,
    )

    # Get all the parameters from the Slack response about the app installation
    (
        access_token,
        bot_access_token,
        bot_user_id,
        webhook_channel,
        webhook_channel_id,
        webhook_configuration_url,
        webhook_url,
        scope,
        team_id,
        team_name,
        user_id,
    ) = get_user_info(response)

    # Get current time to store in dynamodb
    date_installed = datetime.utcnow()

    # Get AWS credentials to save to DynamoDB
    credentials = sts.Boto3STSService(
        arn=CONFIG.dynamodb_role_arn,
        role_name='netacea-slackbot',
        aws_access_key_id=CONFIG.dynamodb_access_key,
        aws_secret_access_key=CONFIG.dynamodb_secret_key,
        region_name='eu-west-1',
        duration=900,
    )

    (
        aws_access_key_id,
        aws_secret_access_key,
        aws_session_token,
    ) = credentials.get_temporary_credentials()

    # Create the dynamodb table model
    dynamo_model = dynamodb.models.NetaceaSlackbotModel()

    # Pass temporary credentials from STS
    dynamo_model.Meta.aws_access_key_id = aws_access_key_id
    dynamo_model.Meta.aws_secret_access_key = aws_secret_access_key
    dynamo_model.Meta.aws_session_token = aws_session_token

    # Update the dynamodb model with the user params
    dynamo_model = dynamodb.functionality.update_model_params(
        model=dynamo_model,
        access_token=access_token,
        bot_access_token=bot_access_token,
        bot_user_id=bot_user_id,
        webhook_channel=webhook_channel,
        webhook_channel_id=webhook_channel_id,
        webhook_configuration_url=webhook_configuration_url,
        webhook_url=webhook_url,
        scope=scope,
        team_id=team_id,
        team_name=team_name,
        user_id=user_id,
        date_installed=date_installed,
    )

    # Save the model in dynamodb
    dynamo_model.save()

    return 'Authentication complete', 200


if __name__ == '__main__':
    app.run()
