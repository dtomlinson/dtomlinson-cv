from typing import Tuple, Union
from uuid import uuid4

from boto3 import session
from botocore import config


class Boto3STSService:
    """docstring for Boto3STSService"""

    def __init__(
        self,
        arn: str,
        aws_access_key_id: str,
        aws_secret_access_key: str,
        region_name: str = 'eu-west-1',
        duration: int = 900,
        role_name: str = uuid4().hex,
        proxy: Union[None, str] = None,
    ) -> None:

        super().__init__()
        self.arn = arn
        self.proxy = proxy
        self.role_name = role_name

        if proxy is not None:
            config = self._get_boto_proxy(self.proxy)
        else:
            config = None

        sess = session.Session(
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            region_name=region_name
        )

        sts_connection = sess.client('sts', config=config)

        self.assume_role_object = sts_connection.assume_role(
            RoleArn=self.arn,
            RoleSessionName=role_name,
            DurationSeconds=duration
        )

    @staticmethod
    def _get_boto_proxy(proxy: str) -> config.Config:
        return config.Config(proxies={'https': proxy})

    def _get_credentials(self):
        self.credentials = self.assume_role_object['Credentials']
        return self

    def get_temporary_credentials(self) -> Tuple[str, str, str]:
        """Method to obtain temporary credentials to log into an AWS resource
        using STS.

        Returns
        -------
        Tuple[str, str, str]
            Returns a tuple containing:

            `aws_access_key_id`
            `aws_secret_access_key`
            `aws_session_token`
        """
        self._get_credentials()
        aws_access_key_id = self.credentials['AccessKeyId']
        aws_secret_access_key = self.credentials['SecretAccessKey']
        aws_session_token = self.credentials['SessionToken']
        return (aws_access_key_id, aws_secret_access_key, aws_session_token)
