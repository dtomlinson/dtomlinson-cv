from importlib import resources
import json

import requests


class LoadResponseMixin():
    """Mixin class to load a response json block."""

    def load_response(
        self, cmd_name: str, cmd_type: str, cmd_level: str, cmd_value: str
    ) -> dict:
        """Load a response from a json file as a python dictionary. The
        parameters can be obtained using
        :function:`~netacea_slackbot.functionality.get_interactivity_params`
        which takes the `action_id` from the json block.

        Parameters
        ----------
        cmd_name : str
            The name of the command.
        cmd_type : str
            The type of response.
        cmd_level : str
            The level of the response.
        cmd_value : str
            The value of the response.

        Returns
        -------

        resp: dict
            The json block loaded as a dictionary.
        """

        response_resource = resources.path(
            f'netacea_slackbot.slackmessageblocks.templates.{cmd_name}',
            f'{cmd_type}_{cmd_level}_{cmd_value}.json'
            )
        with response_resource as r:
            with open(r, 'r') as j:
                resp = json.load(j)
        return resp


class SendResponseMixin():
    """Mixin class to send a json response to Slack."""

    def send_response(
        self, response_url: str, response: dict, in_channel: bool = False
    ):
        """Send a response using POST to the `response_url` with the json
        block `response`. The bool parameter `in_channel` can be used to send
        the response to the channel rather than the default of `ephemeral`.

        Parameters
        ----------
        response_url : str
            URL to send the response to.
        response : dict
            The json block to send as a dict object.
        in_channel : bool, optional
            Setting this to `True` will send the response to everyone in the
            channel. By default will send privately to the user.
        """

        if in_channel:
            response.update({"response_type": "in_channel"})
        else:
            response.update({"response_type": "ephemeral"})

        requests.post(url=response_url, json=response)
