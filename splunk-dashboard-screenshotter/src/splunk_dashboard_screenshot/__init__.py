from time import sleep
import os
import smtplib
from email.mime.multipart import MIMEMultipart
# from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.utils import COMMASPACE, formatdate
# from email import encoders

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotInteractableException


class GetDashboardScreenshot(object):
    def __init__(
        self,
        driver: webdriver,
        url: str,
        username: str,
        password: str,
        width: int = 0,
        height: int = 10000,
        path: str = os.getcwd(),
        delay: int = 1,
    ):
        super().__init__()
        self.driver = driver
        self.action = ActionChains(driver)
        self.url = url
        self.username = username
        self.password = password
        self.width = width
        self.height = height
        self.path = self.checkPath(path)
        self.delay = delay

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.driver.quit()

    @classmethod
    def createDriver(
        cls,
        url: str,
        username: str,
        password: str,
        width: int = 0,
        height: int = 0,
        type: str = 'chromium',
        chromedriver_executable_path: str = 'chromedriver',
        path: str = os.getcwd(),
        delay: int = 1,
    ):
        if type == 'chromium':
            options = webdriver.ChromeOptions()
            options.headless = True
            options.add_argument('--allow-running-insecure-content')
            options.add_argument('--ignore-certificate-errors')
            options.add_argument('--no-sandbox')
            driver = webdriver.Chrome(
                executable_path=chromedriver_executable_path, options=options
            )
        else:
            print(f'No supported browser {type}')
        return cls(driver, url, username, password, width, height, path, delay)

    @staticmethod
    def checkPath(path: str) -> str:
        if path[-1] != '/':
            path += '/'
        return path

    def _setWidth(self, jsProperty: str, override: int):
        script = 'return document.body.parentNode.scroll' + (jsProperty)
        return self.driver.execute_script(script) + override

    def _logIn(self):
        # self.usernameElement = self.driver.find_element_by_xpath(
        #     '//*[@id="username"]'
        # )
        # self.passwordElement = self.driver.find_element_by_xpath(
        #     '/html/body/div[2]/div/div/div[1]/form/fieldset/div[2]/input'
        # )
        self.usernameElement = self.driver.find_element_by_id('username')
        self.passwordElement = self.driver.find_element_by_id('password')
        self.usernameElement.send_keys(self.username)
        self.passwordElement.send_keys(self.password)
        self.driver.find_element_by_xpath(
            '/html/body/div[2]/div/div/div[1]/form/fieldset/input[1]'
        ).click()
        sleep(2)
        return self

    def _hideElements(self):
        try:
            self.driver.find_element_by_class_name(
                'hide-global-filters'
            ).click()
        except (NoSuchElementException, ElementNotInteractableException):
            pass
        try:
            element = self.driver.find_element_by_class_name('dashboard-menu')
            self.driver.execute_script(
                "arguments[0].style.visibility = 'hidden';", element
            )
        except NoSuchElementException:
            pass
        try:
            self.action.move_to_element(
                self.driver.find_element_by_class_name('dashboard-title')
            ).perform()
            sleep(1)
        except NoSuchElementException:
            pass
        return self

    def _goToDashboard(self):
        self.driver.get(self.url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self._logIn()
        return self

    def takeScreenshot(self):
        self._goToDashboard()
        self.driver.set_window_size(
            self._setWidth('Width', self.width),
            self._setWidth('Height', self.height),
        )
        self._hideElements()
        sleep(self.delay)
        # self.driver.find_element_by_tag_name('dashboard-body').screenshot(
        self.driver.find_element_by_tag_name('body').screenshot(
            f'{self.path}web_screenshot.png'
        )
        return self

    @staticmethod
    def sendMail(
        to: list,
        fro: str,
        subject: str,
        # text,
        image: bool = False,
        # files=[],
        server: str = "localhost",
    ):
        assert type(to) == list
        # assert type(files) == list

        msg = MIMEMultipart('related')
        msg['From'] = fro
        msg['To'] = COMMASPACE.join(to)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject

        msg.preamble = 'This is a multi-part message in MIME format.'

        msgAlternative = MIMEMultipart('alternative')
        msg.attach(msgAlternative)
        msgText = MIMEText(
            'William Hill Capacity Reporting. This message is being sent '
            'as plain text.'
        )
        msgAlternative.attach(msgText)

        msgText = MIMEText(
            """<img src="cid:whlogo" style="width: 30%; float: left !important;">
            <h1 style="font-size: 4em;">  </h1>
            <br><br>
            <img src="cid:image1" style="width: 100%; padding-top: 15px;">""",
            'html',
        )
        msgAlternative.attach(msgText)

        if image is True:
            fp = open('web_screenshot.png', 'rb')
            msgImage = MIMEImage(fp.read())
            fp.close()
            msgImage.add_header('Content-ID', '<image1>')
            msg.attach(msgImage)

        fp = open(
            '/opt/bin/docker-binds/splunk-dashboard-screenshotter/email/assets'
            '/wh-logo.png',
            'rb',
        )
        msgImage = MIMEImage(fp.read())
        fp.close()
        msgImage.add_header('Content-ID', '<whlogo>')
        msg.attach(msgImage)

        # for file in files:
        #     part = MIMEBase('application', "octet-stream")
        #     part.set_payload(open(file, "rb").read())
        #     encoders.encode_base64(part)
        #     part.add_header(
        #         'Content-Disposition',
        #         'attachment; filename="%s"' % os.path.basename(file),
        #     )
        #     msg.attach(part)

        smtp = smtplib.SMTP(server)
        smtp.sendmail(fro, to, msg.as_string())
        smtp.close()
