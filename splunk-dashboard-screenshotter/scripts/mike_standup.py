import os

from splunk_dashboard_screenshot import getDashboardScreenshot


def main():

    with getDashboardScreenshot.createDriver(
        url=(
            'https://sc1uxpremn81:8000/en-US/app/wh_weekly_standup/'
            'year_on_year_bets'
        ),
        username='dtomlinson',
        password=os.environ.get('SPLUNK_PASSWORD'),
        width=1920,
        height=10000,
        delay=15,
        path='.',
        chromedriver_executable_path='./chromedriver_79'
    ) as screenShot:
        screenShot.takeScreenshot()


if __name__ == '__main__':
    main()
