# Splunk Dashboard Screenshotter

A python module running in Docker to take a screenshot of a Splunk dashboard and email it on a schedule.


## Version

0.11.0


## Author

Daniel Tomlinson <dtomlinson@williamhill.co.uk>


## Requires 

Docker


## Running

This docker image runs on `sc1uxpremn83` and its config files go in `/opt/bin/docker-binds/splunk-dashboard-screenshotter`.


### Setup

We use the `viki-retailreporting` Splunk dashboard as an example on how to configure a new dashboard. Replace any paths/files with ones you create yourself.

Go to:

`/opt/bin/docker-binds/splunk-dashboard-screenshotter`

Create a folder for your Splunk dashboard:

`/opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting`

In this folder create an `./inputs` and `./outputs` folder:

`/opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting/inputs`
`/opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting/outputs`

In `./inputs` create a python script. This python script should contain:

`./inputs/vicky.py`:

```
from splunk_dashboard_screenshot import GetDashboardScreenshot


def main():

    with GetDashboardScreenshot.createDriver(
        url=(
            'https://10.120.163.81:8000/en-US/app/dashboard-screenshotter/vickyretailstatssaturday'
        ),
        username='$USER',
        password='$PASSWORD',
        width=1000,
        height=1000,
        delay=10,
        path='/outputs',
        chromedriver_executable_path='/usr/local/bin/chromedriver'
    ) as screenShot:
        screenShot.takeScreenshot()


if __name__ == '__main__':
    main()

```

Replace the `url` to the dashboard with the one you want to email.

Remember to replace username and password with the appropiate Splunk user account you've created to automate tasks in Splunk.

In `./outputs` create a python script. This python script should contain:

`./outputs/vicky-email.py`:

```
from splunk_dashboard_screenshot import GetDashboardScreenshot as em

em.sendMail(['dtomlinson@williamhill.co.uk'], 'splunk', 'Retail Status', image=True)
```

Replace the senders with the email addresses you want to send the dashboard to. Edit the `from` and `subject` fields as appropiate. `image=True` should be set to make sure the script picks up the image.

In `./outputs` create a bash script. This bash script should contain:

`./outputs/viki-run.sh`:

```
#!/bin/bash
docker run --rm --net host --name splunk-screenshotter --mount type=bind,src=/opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting/inputs/viki.py,dst=/inputs/viki.py --mount type=bind,src=/opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting/outputs,dst=/outputs -it splunk-dashboard-screenshotter:latest python3.7 /inputs/viki.py

source /opt/bin/docker-binds/splunk-dashboard-screenshotter/email/email/bin/activate

cd /opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting/outputs/

python /opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting/outputs/viki-email.py
```

You should change the `docker run` command and replace the paths with your own script location. You don't need to change the second line - this is activating the email virtual environment which is the same for all dashboards.

A summary of the files you will need to change in the above steps:

`./opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting/inputs/viki.py`

`./opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting/output`

`./opt/bin/docker-binds/splunk-dashboard-screenshotter/viki-retailreporting/outputs/viki-email.py`

