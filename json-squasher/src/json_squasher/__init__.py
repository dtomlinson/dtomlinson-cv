import itertools
from typing import Union, Iterator, Tuple
import json
from pprint import pprint
from copy import deepcopy


class Squash(object):
    """Squash will take any json dictionary and iteratively traverse the tree;
    dynamically flattening the json into one, high-level, and easy to work with
    dictionary.

    This is incredibly useful when working with APIs whose outputs are complex,
    or deeply nested.

    Authors:
        Daniel Tomlinson <dtomlinson@williamhill.co.uk>,
        Alex Bell <alex.bell@williamhill.com>
    Team:
        Capacity

    Attributes
    ----------
    data : dict
        The data to be flattened. This should be a python dictionary and not a
        json string representation.
    """

    def __init__(self, data: dict) -> None:
        """Initialisation parameters

        Parameters
        ----------
        data : dict
            See above for description.
        """
        self.data = data
        super().__init__()

    @property
    def as_dict(self) -> dict:
        """This property will squash the input and return a dictionary.

        Returns
        -------
        dict
            The squashed data as a python dictionary.
        """
        _as_dict = self._squash()
        return _as_dict

    @staticmethod
    def _unpack_dict(
        key: str, value: Union[dict, list, str]
    ) -> Iterator[Tuple[str, Union[dict, list, str]]]:
        """A generator which will yield a dynamically created key and its
        corresponding value.

        This generator will only ever unpack one level at a time. As such
        this is designed to be used with itertools.starmap which will
        iteratively pass its arguments into this generator.

        Parameters
        ----------
        key : str
            The key of a dictionary to be unpacked
        value : Union[dict, list, str]
            The value of a dictionary to be unpacked.

        Yields
        ------
        Iterator[Tuple[str, Union[dict, list, str]]]
            The generator yeilds a str and one of dict, list, or str.

        """
        if isinstance(value, dict):
            for sub_key, sub_value in value.items():
                temporary_key_0 = f'{key}_{sub_key}'
                yield temporary_key_0, sub_value
        elif isinstance(value, list):
            i = 0
            for sub_value in value:
                temporary_key_1 = f'{key}_{str(i)}'
                i += 1
                yield temporary_key_1, sub_value
        else:
            yield key, value

    def _squash(self) -> dict:
        """This method uses itertools.starmap to iteratively squash a nested
        json dictionary. This method will recursively loop until it has
        flattened everything in the original data.

        This method uses itertools.chain.from_iterable to conveniently get
        all elements from each iterable and flatten into a single hirearchy.

        Returns
        -------
        dict
            The squashed data as a python dictionary.
        """
        self.result = deepcopy(self.data)
        while True:
            self.result = dict(
                itertools.chain.from_iterable(
                    itertools.starmap(self._unpack_dict, self.result.items())
                )
            )
            if not any(
                isinstance(value, dict) for value in self.result.values()
            ) and not any(
                isinstance(value, list) for value in self.result.values()
            ):
                break
        return self.result
