Version history
================

1.0.2
------

- Minor fixes and documentation updates.

1.0
--------

- Initial release.
