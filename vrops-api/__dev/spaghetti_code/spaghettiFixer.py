import os
import sys
import json

sys.path.append(os.getcwd())
# from vropsAPI import vropsAPI


# Open files

with open('_vmsResourcesaim.json', 'r+') as jsonFile:
    myDict = json.load(jsonFile)

with open('_vmsResources.json', 'r+') as jsonFile:
    myDictBefore = json.load(jsonFile)

with open('vmid.json', 'r+') as jsonFile:
    vmId = json.load(jsonFile)

with open('vmname.json', 'r+') as jsonFile:
    vmName = json.load(jsonFile)

print(myDict['values'][0])
print()

# Playground

output0 = map(lambda x: x in 'values', myDict)

output1 = map(lambda i: i[0:], myDict)


output2 = [item['name'] for item in myDict['values']]

print(myDictBefore['values'][0])

print()

# output2 = (x for x in myDict(iter(output1)))

print(output2)

print()

name = (
    key
    for item in [item['resourceId'] for item in myDictBefore['values']]
    for key, value in vmId.items()
    if item == value
)


for entry, name in zip(myDictBefore['values'], name):
    entry['name'] = name

print()
print(name)
print()
print(myDictBefore['values'][0])
print(myDictBefore['values'][-2])

"""
names_res = map(lambda i: i['name'][0], data)
ids = [item['owner']['id'] for owner in results['playlists']['items']]
"""
