import requests

url = "https://sc1prapvro01/suite-api//api/resources/stats"

querystring = {"resourceId": "0389a5e4-9b4c-49aa-8139-92ef54f8dd9f",
               "begin": "1564587600000", "end": "1564588200000",
               "statKey":
               ["cpu|workload", "mem|usage_average", "mem|swapped_average"],
               "currentOnly": "True"}

headers = {
    'Authorization': "vRealizeOpsToken f72a2910-88c3-442d-9dfb-58f61aa833fe"
    "::10e2fe6d-55dc-416e-ad3c-9177901f9b3a",
    'Content-Type': "application/json",
    'Accept': "application/json",
    'Cache-Control': "no-cache",
    'Host': "sc1prapvro01",
    'Accept-Encoding': "gzip, deflate",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
}

response = requests.request("GET", url, headers=headers, params=querystring,
                            verify=False)

print(response.text)
