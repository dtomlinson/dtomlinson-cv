import requests

url = "https://sc1prapvro01/suite-api/api/auth/token/acquire"

payload = "{\n\"username\" : \"dtomlinson@Group.WilliamHill.PLC\",\
            \n\"authSource\" : \"whgroup\",\n\"password\" : \"yi3Uw9Mw\"\n}"

headers = {
    'Accept': "application/json",
    'Content-Type': "application/json",
    'Cache-Control': "no-cache",
    'Host': "sc1prapvro01",
    'Accept-Encoding': "gzip, deflate",
    'Content-Length': "102",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }

response = requests.request("POST", url, data=payload, headers=headers,
                            verify=False)

print(response.text)
