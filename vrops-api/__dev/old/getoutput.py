import base64
import json
import os
import sys
sys.path.append(os.getcwd())
from vropsAPI import vropsAPI


# Authenticate:
vrops = vropsAPI.authenticate(
    'https://sc1prapvro01/',
    'svc_splunkVROPS@Group.WilliamHill.PLC',
    'whgroup',
    base64.b64decode(b'UmFjaW5nMjEyMg==').decode(),
    verify=False,
)

# Load VM dict

with open('vms.json', 'r') as file:
    vrops.allVMS = json.load(file)

# Add a single VM to a list to pull back
myList = []
for i in range(0, len(vrops.allVMS)):
    myList.append(list(vrops.allVMS.values())[i])

testList = []
testList.append(myList[10])
testList.append(myList[11])

# print(myList)

# Get data for a vm
vrops.getStatsFromVMS(
    begin=vrops.epochRelativeTime(vrops.epochNow, minutes=-20),
    end=vrops.epochNow,
    intervalType='MINUTES',
    intervalQuantifier='5',
    rollUpType='AVG',
    resourceId=testList,
    statKey=['cpu|usage_average', 'config|hardware|num_Cpu'],
)

# print(vrops.vmsResources)

# Save output to disk
# vrops.saveToDisk(vrops.vmsResources, indent=4, fileName='approach2-output')

export, metric = {}, {}

# All vms to loop through
print(len(vrops.vmsResources['values']))


# vm name
print(vrops.vmsResources['values'][0]['name'])


# data stored in
print(vrops.vmsResources['values'][0]['stat-list']['stat'])

# how many keys there are
print(len(vrops.vmsResources['values'][0]['stat-list']['stat']))

# timestamp earliest, -1 latest?
print(vrops.vmsResources['values'][0]['stat-list']['stat'][0]['timestamps'][0])

# statkey
print(
    vrops.vmsResources['values'][0]['stat-list']['stat'][0]['statKey']['key']
)

# Rolluptype avg etc
print(vrops.vmsResources['values'][0]['stat-list']['stat'][0]['rollUpType'])

# time interval
print(vrops.vmsResources['values'][0]['stat-list']['stat'][0]['intervalUnit'][
    'quantifier'
])

# intervaltype minutes etc
print(vrops.vmsResources['values'][0]['stat-list']['stat'][0]['intervalUnit'][
    'intervalType'
])

# data earliest, -1 latest?
print(vrops.vmsResources['values'][0]['stat-list']['stat'][0]['data'][0])


loopLength = len(vrops.vmsResources['values'])
print(f'loop length - {loopLength}')

timeLength = len(
    vrops.vmsResources['values'][0]['stat-list']['stat'][0]['timestamps']
)

metricLength = len(vrops.vmsResources['values'][0]['stat-list']['stat'])
print(metricLength)

print('\n')

for i in range(0, loopLength):
    # timeLength = len(
    # vrops.vmsResources['values'][1]['stat-list']['stat'][0]['timestamps']
    # )
    # print(json.dumps({'name': vrops.vmsResources['values'][i]['name']}))
    pass

print(
    len(vrops.vmsResources['values'][0]['stat-list']['stat'][0]['timestamps']))

for i in range(0, loopLength):
    statKeyLength = len(vrops.vmsResources['values'][i]['stat-list']['stat'])
    timeLength = len(
        vrops.vmsResources['values'][i]['stat-list']['stat'][0]['timestamps']
    )
    for k in range(0, statKeyLength):
        for j in range(0, timeLength):
            print(type(
                json.dumps({
                    'name': vrops.vmsResources['values'][i]['name'],
                    'timesamp': vrops.vmsResources['values'][i]['stat-list']
                    ['stat'][0]['timestamps'][j],
                    'data': vrops.vmsResources['values'][i]['stat-list']
                    ['stat'][k]['data'][j],
                    'statKey': vrops.vmsResources['values'][i]['stat-list']
                    ['stat'][k]['statKey']['key'],
                    'rollUpType': vrops.vmsResources['values'][i]['stat-list']
                    ['stat'][k]['rollUpType'],
                    'intervalUnit': vrops.vmsResources['values'][i]
                    ['stat-list']['stat'][k]['intervalUnit']['quantifier']
                }),
            ))
