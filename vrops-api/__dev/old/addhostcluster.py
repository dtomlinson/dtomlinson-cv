import base64

# import os
import sys

sys.path.append(
    '/Users/dtomlinson/OneDrive - William Hill Organisation Limited/Mac/'
    'projects/vrops-api/__dev'
)
# sys.path.remove(
#     '/Users/dtomlinson/OneDrive - William Hill Organisation Limited/Mac/'
#     'projects/vrops-api/'
# )
from vropsAPI import vropsAPI


# Authenticate:
vrops = vropsAPI.authenticate(
    'https://sc1prapvro01/',
    'svc_splunkVROPS@Group.WilliamHill.PLC',
    'whgroup',
    base64.b64decode(b'UmFjaW5nMjEyMg==').decode(),
    verify=False,
)


# Get all clusters and store a list of Names:
vrops.getClusters()
vrops.getClusterIdentifiers()
allClustersList = vropsAPI.getKeysList(vrops.allClusters)


# Print all these clusters
print(allClustersList)


# Get all hosts and store a list of Names:
vrops.getHostsFromCluster(cluster='SC1PRCONTXWHCUXCCL01')
vrops.getHostIdentifiers()
allHostsList = vrops.getKeysList(vrops.allHosts)


# Print all these hosts
print(allHostsList)

# # Add just 2 hots
# counter = 0
# hostList = []
# for host in allHostsList:
#     if counter == 2:
#         break
#     else:
#         hostList.append(host)
#         counter += 1


# Get all VMs and sore a list of IDs
vrops.getVMSFromHost(allHostsList)
vrops.getVMSIdentifiers()
allVMSIdList = vrops.getValuesList(vrops.allVMS)


# Save all VMs to disk
vrops.saveToDisk(vrops.allVMS, indent=4, filePrefix='approach1-vms')


# Get data for a vm
vrops.getStatsFromVMS(
    begin=vrops.epochRelativeTime(vrops.epochNow, minutes=-11),
    end=vrops.epochNow,
    intervalType='MINUTES',
    intervalQuantifier='5',
    rollUpType='AVG',
    resourceId=allVMSIdList,
    statKey=['cpu|usage_average', 'config|hardware|num_Cpu'],
)


# Export the data into readable format
vrops.exportVMData()


# Save to disk
vrops.saveToDisk(
    vrops.export,
    indent=4,
    filePrefix='approach1-export',
    breakLine=True,
    path='/Users/dtomlinson/OneDrive - William Hill Organisation Limited'
    '/Mac/projects/vrops-api/__dev',
)
