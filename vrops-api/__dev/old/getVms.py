import requests
import json
import math

url = "https://sc1prapvro01/suite-api/api/resources"

querystring = {"page": "0", "regex": "^prdx*"}

headers = {
    'Content-Type': "application/json",
    'Authorization': "vRealizeOpsToken f72a2910-88c3-442d-9dfb-58f61aa833fe"
    "::29abe2bf-a1f7-464d-b48a-508320a98627",
    'Content-Type': "application/json",
    'Accept': "application/json",
    'Cache-Control': "no-cache",
    'Host': "sc1prapvro01",
    'Accept-Encoding': "gzip, deflate",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
}

response = requests.request("GET", url, headers=headers,
                            params=querystring, verify=False)

t = json.loads(response.text)
listNames = []
totalVms = (t['pageInfo']['totalCount'])
vmsPerPage = (t['pageInfo']['pageSize'])
pages = math.ceil(totalVms/vmsPerPage)

queryList = [i for i in range(0, pages)]

for page in queryList:
    querystring = {'page': page}
    response = requests.request("GET", url, headers=headers,
                                params=querystring, verify=False)
    for i in (range(0, 1000)):
        t = json.loads(response.text)
        listNames.append(t['resourceList'][i]['resourceKey']['name'])
        print(listNames[i])

print(listNames)
