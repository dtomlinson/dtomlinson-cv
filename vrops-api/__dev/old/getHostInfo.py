import base64
import os
import sys

sys.path.append(os.getcwd())
from vropsAPI import vropsAPI


# Authenticate:
vrops = vropsAPI.authenticate(
    'https://sc1prapvro01/',
    'svc_splunkVROPS@Group.WilliamHill.PLC',
    'whgroup',
    base64.b64decode(b'UmFjaW5nMjEyMg==').decode(),
    verify=False,
)


# Get all clusters and store a list of Names:
vrops.getClusters()
vrops.getClusterIdentifiers()
allClustersList = vrops.getKeysList(vrops.allClusters)


# Print all these clusters
print(allClustersList)

# Get all hosts and store a list of Names:
vrops.getHostsFromCluster(cluster='SC1PRCONTXWHCUXCCL01')
vrops.getHostIdentifiers()
allHostsList = vrops.getValuesList(vrops.allHosts)


# # Print all these hosts
# print(allHostsList)


# # Get all VMs and sore a list of IDs:
# vrops.getVMSFromHost(allHostsList)
# vrops.getVMSIdentifiers()
# allVMSIdList = vrops.getValuesList(vrops.allVMS)

# # Save all VMs to disk
# vrops.saveToDisk(vrops.allVMS, indent=4, filePrefix='approach1-vms')

# # Save all VMs:Hosts to disk
# vrops.saveToDisk(
#     vrops.VMSHostsNames, indent=4, filePrefix='approach1-vms_hosts'
# )


# # Add a single VM to a list to pull back
# myList = []
# myList.append(allVMSIdList[0])


# Get data for a vm
vrops.getStatsFromVMS(
    begin=vrops.epochRelativeTime(
        vrops.epochNow, days=-3, hour=15, minute=0, second=0
    ),
    end=vrops.epochRelativeTime(
        vrops.epochNow, days=-3, hour=17, minute=0, second=0
    ),
    intervalType='MINUTES',
    intervalQuantifier='5',
    rollUpType='AVG',
    resourceId=allHostsList,
    statKey=['cpu|corecount_provisioned'],
    vropsType='host',
)


# Export the data into readable format
vrops.exportVMData()

# Save to disk
vrops.saveToDisk(
    vrops._vmsResources,
    indent=4,
    filePrefix='reddit-help-before',
    breakLine=True,
)

vrops.saveToDisk(
    vrops.export, indent=4, filePrefix='reddit-help-after', breakLine=True
)
