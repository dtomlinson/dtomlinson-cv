<!-- MarkdownTOC -->

- [Added](#added)
- [Info](#info)

<!-- /MarkdownTOC -->


# Added
- self.chosenCluster in getHostsFromCLuster


# Info

```host info in {'pageInfo': {'totalCount': 37, 'page': 0, 'pageSize': 1000}, 'links': [{'href': '/suite-api/api/resources/15b3ea0c-9f62-4fc2-93b8-d4281196043e/relationships?page=0&amp;pageSize=1000', 'rel': 'SELF', 'name': 'current'}, ```

 in `self._vmsResources[i]['resourceList']`



if ```'resourceKey': {'name': 'SC1PRCONTXWHCUXCCL01', 'adapterKindKey': 'VMWARE', 'resourceKindKey': 'ClusterComputeResource', ```

if this is cluster compute resource, extract the name
