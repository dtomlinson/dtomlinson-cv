import base64
import json
import os
import sys

sys.path.append(os.getcwd())
from vropsAPI import vropsAPI


# Authenticate:
vrops = vropsAPI.authenticate(
    'https://sc1prapvro01/',
    'svc_splunkVROPS@Group.WilliamHill.PLC',
    'whgroup',
    base64.b64decode(b'UmFjaW5nMjEyMg==').decode(),
    verify=False,
)


# Load VM dict
with open('approach1-vms-example.json', 'r') as vms:
    vrops.allVMS = json.load(vms)


# Get list of these VMS
allVMSIdList = vrops.getValuesList(vrops.allVMS)

# Load VM_Host dict
with open('approach1-vms_hosts-example.json', 'r') as vmsHosts:
    vrops.VMSHostsNames = json.load(vmsHosts)


# Set the cluster
vrops.chosenCluster = 'SC1PRCONTXWHCUXCCL01'


# # Add a single VM to a list to pull back (can add more VMs to this list if
# # needed)
# myList = []
# myList.append(list(vrops.allVMS.values())[100])


# Get data for a vm
vrops.getStatsFromVMS(
    begin=vrops.epochRelativeTime(vrops.epochNow, minutes=-30),
    end=vrops.epochNow,
    intervalType='MINUTES',
    intervalQuantifier='1',
    rollUpType='AVG',
    resourceId=allVMSIdList,
    statKey=['rescpu|actav1_latest'],
)


# Export the data into readable format
vrops.exportVMData()


print(vrops.export)

# Save to disk
vrops.saveToDisk(
    vrops.export, indent=4, filePrefix='approach2-export', breakLine=True
)
